// First problem:
let number = Number(prompt(`Give me a number`))
console.log(`The number you provided is: 👉${number}👈`)

for (let count = number; count >= 0; count--) {
  if (count <= 50) {
    console.log(`💀 The current value is at ${count}. Terminating the loop 💀`)
    break
  } else if (count % 10 === 0) {
    console.log(`👉${count}👈 is divisible by 10: ❌skipping number❌`)
    continue
  } else if (count % 5 === 0) {
    console.log(`👉${count}👈 is divisible by 5 ✅`)
  }
}

// Second problem:
// supercalifragilisticexpialidocious
let string = String(prompt(`Give me a string`))
string = string.toLowerCase()
console.log(`\nThe string that you provided is:
👉${string}👈`)

let newString = ''
for (let zero = 0; zero < string.length; zero++) {
  if (
    string[zero] === 'a' ||
    string[zero] === 'e' ||
    string[zero] === 'i' ||
    string[zero] === 'o' ||
    string[zero] === 'u'
  )
    continue
  else {
    let consonant = string[zero]
    newString += consonant
  }
}
console.log(`Your string without the vowels: 
👉${newString}👈`)
