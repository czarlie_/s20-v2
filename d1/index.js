console.log(`Hola Mundo 🌎`)

/*
	Mini Activity
	1) Create a function named greeting() and display the message you want to say to yourself using console log inside of the function
	2) Invoke the greeting() 20 times
	3) Take a screenshot

*/

function greeting() {
  console.log(`Hola Amigo 🧑‍🤝‍🧑`)
}
greeting()
greeting()
greeting()
greeting()
greeting()
greeting()
greeting()
greeting()
greeting()
greeting()

let countNum = 20

while (countNum !== 0) {
  console.log(`This is printed inside the loop ${countNum}`)
  greeting()
  countNum--
}

/*
	A while loop takes in an expression/condition
	- expressions are any unit of code that can be
	evaluated to a value
	-if the condition evaluates to be true, the	statement insidethe code block will be executed.
	- a statement is a command that the programmer gives to a computer

	* A loop will iterate a certian number of times an expression or a condition is met
	
	- iteration is the term given to the repetition	of statements.

	Syntax:
	while (expression/condition) {
		statement
	}
*/

let count = 5

while (count !== 0) {
  console.log(`While: ${count}`)
  count--
}

/*
	A do while loop works a lot like while loop, but unlike while loops, do-while loops guarantee that	the code will be executed once.

	Syntax:
	do {
		statement
	} while (expression/condition)

*/

let number = Number(prompt(`Give me a number`))

do {
  console.log(`Do-while: ${number}`)
  number++
} while (number < 10)

/*
	- A for loop is more flexible than while and do-while loops. It consists of three parts:
		1. The "initialization" value that will track the progression of the loop.
		2. The "expression/condition" that will be evaluated which will determine whether the loop will run one more time.
		3. The "FihalExpression" indicates how tojadvance the loop.
	- Syntax
	for (initialization; expression/condition; finalExpression) {
	statement

*/

for (let count = 0; count <= 100; count++) {
  if (count % 2 === 0) console.log(count)
}

/*
	mini ackvity
	refactor the code above that will only print the even numbers
	Take a screenshot of your work's console and send it to our batch chat
*/

let myString = `Vice Ganda`

console.log(myString.length)

console.log(myString[0])
console.log(myString[1])
console.log(myString[2])

for (let x = 0; x < myString.length; x++) console.log(myString[x])

let myName = `Zeref Dragneel`
for (let i = 0; i < myName.length; i++) {
  if (
    myName[i].toLowerCase() == `a` ||
    myName[i].toLowerCase() == `e` ||
    myName[i].toLowerCase() == `i` ||
    myName[i].toLowerCase() == `o` ||
    myName[i].toLowerCase() == `u`
  ) {
    console.log(`Hi I'm a vowel that orbits! 🛰️`)
  } else console.log(myName[i])
}

// The break statement is used to terminal the current loop once a match has been found

for (let count = 0; count <= 20; count++) {
  if (count % 2 === 0) continue
  console.log(`Continue and Break ${count}`)
}

let name = 'Mommy Dragneel Yonisia'

for (let i = 0; i < name.length; i++) {
  console.log(name[i])
  //if the vowel is equal to a, continue to the next iteration of the loop
  if (name[i].toLowerCase === 'a') {
    console.log('Continue to the next iteration')
    continue
  }
  if (name[i] == 'Y') console.log(`tenshi`)
}
